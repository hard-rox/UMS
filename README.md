# University Management System
This web application is only for using one semester only of a university. This project is partial fulfillment of course ASP.NET MVC in BITM.
See the [Project requirment Doc](https://gitlab.com/hard-rox/UMS/tree/master/Resources/Specification of University Course and Result Management System.pdf)
## Getting Started

### Prerequisites

To run this application you need
* .NET 4.6
* SQL Server 2012
in your Computer.

## Built With

* ASP.NET MVC
* Visual C#
* SQL Server 2012
* .NET Framework 4.6
* Layer Architecture
* Visual Studio 2013

## Authors

* **Rasedur Rahman Roxy** - [In Facebook](https://facebook.com/MdRoxy)