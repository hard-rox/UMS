﻿using System;
using System.Collections.Generic;
using UniversityManagementSystem.DAL;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.BLL
{
    public class CourseTeacherManager
    {
        private CourseTeacherGateway courseTeacherGateway = new CourseTeacherGateway();
        private CourseGateway courseGateway = new CourseGateway();
        private TeacherGateway teacherGateway = new TeacherGateway();
        public string AssignCourse(int courseId, int teacherId)
        {
            try
            {
                Course course = courseGateway.GetCourseById(courseId);
                Teacher teacher = teacherGateway.GetTeacherById(teacherId);
                if (courseTeacherGateway.AssignTeacher(course, teacher))
                    return "Course Assigned.";
                return "Course assignment failed.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public List<CourseTeacherViewModel> GetCourseStaticByDept(int deptId)
        {
            return courseTeacherGateway.GetCourseStaticByDept(deptId);
        }

        public string UnassignAllCourses()
        {
            try
            {
                if (courseTeacherGateway.UnassignAllCourses()) return "All course unassigned.";
                return "Unassignment failed.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}