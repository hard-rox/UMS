﻿using System.Collections.Generic;
using UniversityManagementSystem.DAL;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.BLL
{
    public class DepartmentManager
    {
        DepartmentGateway departmentGateway=new DepartmentGateway();

        public string SaveDepartment(Department department)
        {
            if (departmentGateway.IsDuplicate(department)) return "Name & Code must be Unique.";
            if (departmentGateway.SaveDepartment(department)) return "Saved Successfully.";
            return "Failed to save.";
        }

        public List<Department> GetallDepartments()
        {
            return departmentGateway.GetAllDepartments();
        } 
    }
}