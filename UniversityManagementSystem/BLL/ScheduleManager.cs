﻿using System;
using System.Collections.Generic;
using UniversityManagementSystem.DAL;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.BLL
{
    public class ScheduleManager
    {
        private ScheduleGateway scheduleGateway = new ScheduleGateway();

        public string AllocateRoom(Schedule schedule)
        {
            try
            {
                if (schedule.FromTime >= schedule.ToTime) return "Invalid Time.";
                if (scheduleGateway.Overlap(schedule)) return "Time overlaps.";
                if (scheduleGateway.AllocateRoom(schedule)) return "Room allocated.";
                return "Error occured.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public List<Schedule> GetScheduleByDept(int deptId)
        {
            return scheduleGateway.GetScheduleByDept(deptId);
        }

        public List<Schedule> GetAllRooms()
        {
            return scheduleGateway.GetAllRooms();
        }

        public string UnallocateAllRooms()
        {
            try
            {
                if (scheduleGateway.UnallocateAllRooms()) return "All rooms unallocated.";
                return "Unallocation failed.";
            }
            catch (Exception ex)
            {
                return ex.Message;
                throw;
            }
        }
    }
}