﻿using System;
using System.Collections.Generic;
using UniversityManagementSystem.DAL;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.BLL
{
    public class CourseManager
    {
        private CourseGateway courseGateway = new CourseGateway();

        public string SaveCourse(Course course)
        {
            try
            {
                if (courseGateway.IsDuplicate(course)) return "Course Code & Name must be unique.";
                if (courseGateway.SaveCourse(course)) return "Saved successfully.";
                return "Save failed.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public List<Course> GetAllCourse()
        {
            return courseGateway.GetAllCourse();
        }
        public List<Course> GetUnassignedCourse(int deptId)
        {
            return courseGateway.GetUnassignedCourse(deptId);
        }

        public List<Course> GetCourseByDept(int deptId)
        {
            return courseGateway.GetCourseByDept(deptId);
        }

        public Course GetCourseById(int id)
        {
            return courseGateway.GetCourseById(id);
        }

        public List<Course> GetCourseByStudent(Student student)
        {
            return courseGateway.GetCourseByStudent(student);
        }
    }
    
}