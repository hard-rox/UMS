﻿using System.Collections.Generic;
using UniversityManagementSystem.DAL;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.BLL
{
    public class SemesterManager
    {
        SemesterGateway semesterGateway=new SemesterGateway();

        public List<Semester> GetAllSemesters()
        {
            return semesterGateway.GetAllSemesters();
        }
    }
}