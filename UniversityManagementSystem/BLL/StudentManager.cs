﻿using System;
using System.Collections.Generic;
using UniversityManagementSystem.DAL;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.BLL
{
    public class StudentManager
    {
        StudentGateway studentGateway=new StudentGateway();
        private DepartmentGateway departmentGateway = new DepartmentGateway();
        public string SaveStudent(Student student)
        {
            string xxx=(studentGateway.GetNumofStudent(student.DepartmentName.Id)+1).ToString("D"+3);
            string code = departmentGateway.GetCode(student.DepartmentName.Id);
            student.RegistrationNo =  code+ "-" + student.RegistrationDate.Year + "-" + xxx;
            try
            {
                if (studentGateway.IsDuplicate(student)) return "Email already entered.";
                if (studentGateway.SaveStudent(student)) return "Registered successfully.";
                return "Save failed.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public List<Student> GetAllstStudents()
        {
            return studentGateway.GetAllstStudents();
        }

        public Student GetStudentById(int id)
        {
            return studentGateway.GetStStudentById(id);
        }
    }
}