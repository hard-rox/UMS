﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Configuration;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.DAL
{
    public class DepartmentGateway
    {
        private static string conString = WebConfigurationManager.ConnectionStrings["UniversityCon"].ConnectionString;
        SqlConnection _universityCon=new SqlConnection(conString);

        public bool SaveDepartment(Department department)
        {
            string query = "insert into Department(Code, Name) values('" + department.Code + "','" + department.Name +
                           "');";
            _universityCon.Open();
            SqlCommand saveCommand = new SqlCommand(query, _universityCon);
            int rowCount = saveCommand.ExecuteNonQuery();
            _universityCon.Close();
            if (rowCount > 0) return true;
            return false;
        }

        public bool IsDuplicate(Department department)
        {
            string query = "select * from Department where Code='"+department.Code+"' or Name='"+department.Name+"';";
            _universityCon.Open();
            SqlCommand checkCommand = new SqlCommand(query, _universityCon);
            SqlDataReader duplicateReader = checkCommand.ExecuteReader();
            bool duplicate = duplicateReader.HasRows;
            duplicateReader.Close();
            _universityCon.Close();
            return duplicate;
        }

        public List<Department> GetAllDepartments()
        {
            List<Department> departments=new List<Department>();

            string query = "select * from Department;";
            _universityCon.Open();
            SqlCommand getCommand = new SqlCommand(query, _universityCon);
            SqlDataReader getReader= getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    departments.Add(new Department()
                    {
                        Id = Convert.ToInt32(getReader["Id"]),
                        Code = getReader["Code"].ToString(),
                        Name = getReader["Name"].ToString()
                    });
                }
            }
            getReader.Close();
            _universityCon.Close();
            return departments;
        }

        public string GetCode(int deptId)
        {
            string query =
                @"select Code from Department where Id=" + deptId + ";";
            _universityCon.Open();
            SqlCommand saveCommand = new SqlCommand(query, _universityCon);
            var code = saveCommand.ExecuteScalar().ToString();
            _universityCon.Close();
            return code;
        }
    }
}