﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Configuration;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.DAL
{
    public class CourseTeacherGateway
    {
        private static string conString = WebConfigurationManager.ConnectionStrings["UniversityCon"].ConnectionString;
        SqlConnection _universityCon = new SqlConnection(conString);
        
        public bool AssignTeacher(Course course, Teacher teacher)
        {
            string query1 = @"insert into CourseTeacher(CourseId,TeacherId) values("+course.Id+","+teacher.Id+");";
            string query2 = @"update Teacher set CreditTaken=CreditTaken+"+course.Credit+" where Id="+teacher.Id+";";
            string query3 = @"update Course set IsAssigned='True' where Id= " + course.Id + ";";
            _universityCon.Open();
            SqlCommand saveCommand = new SqlCommand(query1, _universityCon);
            int rowCount = saveCommand.ExecuteNonQuery();
            _universityCon.Close();
            if (rowCount > 0)
            {
                _universityCon.Open();
                saveCommand = new SqlCommand(query2, _universityCon);
                saveCommand.ExecuteNonQuery();
                _universityCon.Close();

                _universityCon.Open();
                saveCommand = new SqlCommand(query3, _universityCon);
                saveCommand.ExecuteNonQuery();
                _universityCon.Close();
                return true;
            }
            return false;
        }

        public List<CourseTeacherViewModel> GetCourseStaticByDept(int deptId)
        {
            List<CourseTeacherViewModel> courseStatics = new List<CourseTeacherViewModel>();

            string query = "select * from CourseStaticsView where DeptId="+deptId+";";
            _universityCon.Open();
            SqlCommand getCommand = new SqlCommand(query, _universityCon);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    courseStatics.Add(new CourseTeacherViewModel()
                    {
                        CourseCode = getReader["Code"].ToString(),
                        CourseName = getReader["Name"].ToString(),
                        CourseSemester = getReader["Semester"].ToString(),
                        TeacherName = getReader["Teacher"].ToString()
                    });
                }
            }
            getReader.Close();
            _universityCon.Close();
            return courseStatics;
        }

        public bool UnassignAllCourses()
        {
            string query1 = "update CourseTeacher set IsDeleted='True';";
            string query2 = "update Course set IsAssigned='False';";
            string query3 = "update Teacher set CreditTaken=0;";
            _universityCon.Open();
            SqlCommand unassignCommand = new SqlCommand(query1, _universityCon);
            int rowCount = unassignCommand.ExecuteNonQuery();
            unassignCommand = new SqlCommand(query2, _universityCon);
            unassignCommand.ExecuteNonQuery();
            unassignCommand = new SqlCommand(query3, _universityCon);
            unassignCommand.ExecuteNonQuery();
            _universityCon.Close();
            if (rowCount > 0)
                return true;
            return false;
        }
    }
}