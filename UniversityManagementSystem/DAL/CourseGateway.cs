﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Configuration;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.DAL
{
    public class CourseGateway
    {
        private static string conString = WebConfigurationManager.ConnectionStrings["UniversityCon"].ConnectionString;
        SqlConnection _universityCon = new SqlConnection(conString);

        public bool SaveCourse(Course course)
        {
            string query = @"insert into Course(Code, Name, Credit, Description, Depertment, Semester)
                            values('" + course.Code + "', '" + course.Name + "'," + course.Credit + ", '" +
                           course.Description + "'," + course.DepartmentId + "," + course.SemesterId + "); ";
            _universityCon.Open();
            SqlCommand saveCommand = new SqlCommand(query, _universityCon);
            int rowCount = saveCommand.ExecuteNonQuery();
            _universityCon.Close();
            if (rowCount > 0) return true;
            return false;
        }

        public bool IsDuplicate(Course course)
        {
            string query = "select * from Course where Code='" + course.Code + "' or Name='" + course.Name + "';";
            _universityCon.Open();
            SqlCommand checkCommand = new SqlCommand(query, _universityCon);
            SqlDataReader duplicateReader = checkCommand.ExecuteReader();
            bool duplicate = duplicateReader.HasRows;
            duplicateReader.Close();
            _universityCon.Close();
            return duplicate;
        }

        public List<Course> GetAllCourse()
        {
            List<Course> courses = new List<Course>();

            string query = "select * from Course;";
            _universityCon.Open();
            SqlCommand getCommand = new SqlCommand(query, _universityCon);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    courses.Add(new Course()
                    {
                        Id = Convert.ToInt32(getReader["Id"]),
                        Code = getReader["Code"].ToString(),
                        Name = getReader["Name"].ToString()
                    });
                }
            }
            getReader.Close();
            _universityCon.Close();
            return courses;
        }


        public List<Course> GetUnassignedCourse(int deptId)
        {
            List<Course> courses = new List<Course>();

            string query = "select * from Course Where Depertment=" + deptId + " and IsAssigned='False';";
            _universityCon.Open();
            SqlCommand getCommand = new SqlCommand(query, _universityCon);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    courses.Add(new Course()
                    {
                        Id = Convert.ToInt32(getReader["Id"]),
                        Code = getReader["Code"].ToString(),
                        Name = getReader["Name"].ToString()
                    });
                }
            }
            getReader.Close();
            _universityCon.Close();
            return courses;
        }

        public List<Course> GetCourseByDept(int deptId)
        {
            List<Course> courses = new List<Course>();

            string query = "select * from Course where Depertment=" + deptId + ";";
            _universityCon.Open();
            SqlCommand getCommand = new SqlCommand(query, _universityCon);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    courses.Add(new Course()
                    {
                        Id = Convert.ToInt32(getReader["Id"]),
                        Code = getReader["Code"].ToString(),
                        Name = getReader["Name"].ToString(),
                        Credit = Convert.ToDouble(getReader["Credit"])
                    });
                }
            }
            getReader.Close();
            _universityCon.Close();
            return courses;
        }

        public Course GetCourseById(int id)
        {
            Course course = new Course();
            string query = "select * from Course where Id=" + id + ";";
            _universityCon.Open();
            SqlCommand getCommand = new SqlCommand(query, _universityCon);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    course.Id = Convert.ToInt32(getReader["Id"]);
                    course.Code = getReader["Code"].ToString();
                    course.Name = getReader["Name"].ToString();
                    course.Credit = Convert.ToDouble(getReader["Credit"]);
                }
            }
            getReader.Close();
            _universityCon.Close();
            return course;
        }

        public List<Course> GetCourseByStudent(Student student)
        {
            List<Course> courses = new List<Course>();

            string query =
                "select Id, Code, Name, Credit from Course where Depertment= " + student.DepartmentName.Id
                + " except"
                + " select Course.Id, Course.Code, Course.Name, Course.Credit"
                + " from Course join CourseEnroll on Course.Id = CourseEnroll.CourseId"
                + " where CourseEnroll.StudentId = " + student.Id
                + " and(CourseEnroll.Grade is null"
                + " or CourseEnroll.Grade != 'F')";

            _universityCon.Open();
            SqlCommand getCommand = new SqlCommand(query, _universityCon);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    courses.Add(new Course()
                    {
                        Id = Convert.ToInt32(getReader["Id"]),
                        Name = getReader["Name"].ToString(),
                    });
                }
            }
            getReader.Close();
            _universityCon.Close();
            return courses;
        }
    }
}