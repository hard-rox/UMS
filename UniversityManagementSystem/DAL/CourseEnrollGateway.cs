﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Configuration;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.DAL
{
    public class CourseEnrollGateway
    {
        private static string conString = WebConfigurationManager.ConnectionStrings["UniversityCon"].ConnectionString;
        SqlConnection _universityCon = new SqlConnection(conString);

        public bool EnrollCourse(CourseEnrollViewModel courseEnrollView)
        {
            string query1 = @"insert into CourseEnroll(StudentId,CourseId, Date) values(" + courseEnrollView.StudentId + "," + courseEnrollView.CourseId + ", '"+courseEnrollView.Date.ToShortDateString()+"');";
            _universityCon.Open();
            SqlCommand saveCommand = new SqlCommand(query1, _universityCon);
            int rowCount = saveCommand.ExecuteNonQuery();
            _universityCon.Close();
            if (rowCount > 0) return true;
            return false;
        }

        public List<Student> GetEnrolledStudents()
        {
            List<Student> students = new List<Student>();

            string query = @"select Student.Id,Student.RegistrationNo, Student.Name,Email,Department.Name as DName
                            from Student, Department
                            where Student.Department=Department.Id
                            and exists (select StudentId from CourseEnroll where StudentId=Student.Id);";
            _universityCon.Open();
            SqlCommand getCommand = new SqlCommand(query, _universityCon);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    students.Add(new Student()
                    {
                        Id = Convert.ToInt32(getReader["Id"]),
                        RegistrationNo = getReader["RegistrationNo"].ToString(),
                        Name = getReader["Name"].ToString(),
                        Email = getReader["Email"].ToString(),
                        DepartmentName = new Department()
                        {
                            Name = getReader["DName"].ToString()
                        }
                    });
                }
            }
            getReader.Close();
            _universityCon.Close();
            return students;
        }
        public List<Course> GetEnrolledCourseByStudentId(int studentId)
        {
            List<Course> courses = new List<Course>();

            string query = @"select Course.Id, Course.Name
                            from Student,Course, CourseEnroll
                            where Student.Id=CourseEnroll.StudentId
                            and Course.Id=CourseEnroll.CourseId
                            and CourseEnroll.StudentId=" + studentId +
							" and CourseEnroll.Grade is null;";
            _universityCon.Open();
            SqlCommand getCommand = new SqlCommand(query, _universityCon);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    courses.Add(new Course()
                    {
                        Id = Convert.ToInt32(getReader["Id"]),
                        Name = getReader["Name"].ToString(),
                    });
                }
            }
            getReader.Close();
            _universityCon.Close();
            return courses;
        }

        public List<CourseEnrollViewModel> GetResultById(int studentId)
        {
            List<CourseEnrollViewModel> courses = new List<CourseEnrollViewModel>();

            string query = @"select * from ViewResult where StudentId="+studentId+";";
            _universityCon.Open();
            SqlCommand getCommand = new SqlCommand(query, _universityCon);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    courses.Add(new CourseEnrollViewModel()
                    {
                        Code = getReader["Code"].ToString(),
                        CourseName = getReader["Name"].ToString(),
                        Grade = getReader["Grade"].ToString(),
                    });
                }
            }
            getReader.Close();
            _universityCon.Close();
            return courses;
        }

        public bool SaveResult(int studentId, int courseId, string grade)
        {
            string query1 = @"update CourseEnroll " +
                                "set Grade='" + grade + "' " +
                                "where StudentId=" + studentId + " " +
                                "and CourseId=" + courseId + " " +
                                "and Grade is null;";
                                
            _universityCon.Open();
            SqlCommand saveCommand = new SqlCommand(query1, _universityCon);
            int rowCount = saveCommand.ExecuteNonQuery();
            _universityCon.Close();
            if (rowCount > 0) return true;
            return false;
        }

        public Course GetIncompletePrerequisite(int studentId, int courseId)
        {
            var query = @"select Course.Id, Course.Code, Course.Name, Course.Credit
                from Course join CourseEnroll on Course.Id=CourseEnroll.CourseId
                where  CourseEnroll.StudentId= " + studentId
                    + " and Course.Id = (select Prerequisite from Course where Id = " + courseId + ")"
                    + " and (CourseEnroll.Grade is null"
                    + " or CourseEnroll.Grade = 'F')";

            _universityCon.Open();
            var getCommand = new SqlCommand(query, _universityCon);
            var reader = getCommand.ExecuteReader();

            if (reader.HasRows)
            {
                var prereq = new Course();
                while (reader.Read())
                {
                    prereq.Id = Convert.ToInt32(reader["Id"]);
                    prereq.Code = reader["Code"].ToString();
                    prereq.Name = reader["Name"].ToString();
                    prereq.Credit = Convert.ToDouble(reader["Credit"]);
                }
                return prereq;
            }
            return null;
        }
    }
}