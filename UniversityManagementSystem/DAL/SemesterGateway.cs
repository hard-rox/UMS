﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Configuration;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.DAL
{
    public class SemesterGateway
    {
        private static string conString = WebConfigurationManager.ConnectionStrings["UniversityCon"].ConnectionString;
        SqlConnection _universityCon = new SqlConnection(conString);

        public List<Semester> GetAllSemesters()
        {
            List<Semester> semesters = new List<Semester>();

            string query = "select * from Semester;";
            _universityCon.Open();
            SqlCommand getCommand = new SqlCommand(query, _universityCon);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    semesters.Add(new Semester()
                    {
                        Id = Convert.ToInt32(getReader["Id"]),
                        Title = getReader["Title"].ToString()
                    });
                }
            }
            getReader.Close();
            _universityCon.Close();
            return semesters;
        }
    }
}