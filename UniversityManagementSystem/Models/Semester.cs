﻿namespace UniversityManagementSystem.Models
{
    public class Semester
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}