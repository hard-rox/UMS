﻿using System;

namespace UniversityManagementSystem.Models
{
    public class Schedule
    {
        public int CourseId { get; set; }
        public string CourseCode { get; set; }
        public string CourseName { get; set; }

        public int RoomId { get; set; }
        public string RoomNo { get; set; }

        public string Day { get; set; }

        public DateTime FromTime { get; set; }
        public DateTime ToTime { get; set; }
        public string ScheduleInfo { get; set; }
    }
}