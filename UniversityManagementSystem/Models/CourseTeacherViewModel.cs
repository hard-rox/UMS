﻿using System.Collections;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace UniversityManagementSystem.Models
{
    public class CourseTeacherViewModel
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int CourseId { get; set; }
        [Required]
        [MinLength(5, ErrorMessage = "Code must be at least 5 character.")]
        [DisplayName("Code")]
        public string CourseCode { get; set; }
        [Required]
        [DisplayName("Name/Title")]
        public string CourseName { get; set; }
        [DisplayName("Semester")]
        public string CourseSemester { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int TeacherId { get; set; }
        [Required]
        [DisplayName("Assigned To")]
        public string TeacherName { get; set; }

    }
}