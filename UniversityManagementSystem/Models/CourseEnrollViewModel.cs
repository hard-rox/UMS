﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace UniversityManagementSystem.Models
{
    public class CourseEnrollViewModel
    {

        [Required]
        [Range(1,int.MaxValue)]
        public int StudentId { get; set; }
        [Required]
        public int CourseId { get; set; }
        [DisplayName("Course Code")]
        public string Code { get; set; }
        [Required]
        public string CourseName { get; set; }

        public string Grade { get; set; }
        [Required]
        [DisplayName("Date")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
    }
}