﻿using System.Linq;
using System.Web.Mvc;
using UniversityManagementSystem.BLL;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Controllers
{
    public class StudentController : Controller
    {
        private DepartmentManager departmentManager = new DepartmentManager();
        private StudentManager studentManager = new StudentManager();
        private AppUserManager appUserManager = new AppUserManager();

        // GET: Student
        [Authorize(Roles = "Teacher")]
        public ActionResult Index()
        {
            ViewBag.Department = departmentManager.GetallDepartments();

            ViewBag.Title = "Register Student";
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Teacher")]
        public ActionResult Index(Student student)
        {
            //student.UserId = user.Id;
            appUserManager.CreateUser(student);
            ViewBag.Msg = studentManager.SaveStudent(student);
            var departments = departmentManager.GetallDepartments();
            ViewBag.Department = departments;
            ViewBag.Title = "Register Student";
            if (ViewBag.Msg != "Registered successfully.") return View();

            student.DepartmentName = departments.First(e => e.Id == student.DepartmentName.Id);
            return View("Details", student);
        }

        // GET: Student/Details/5
        public ActionResult Details()
        {
            return View();
        }
    }
}
