﻿using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.pdf;
using UniversityManagementSystem.BLL;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Controllers
{
    public class CourseEnrollController : Controller
    {
        private StudentManager studentManager = new StudentManager();
        private CourseManager courseManager = new CourseManager();
        private CourseEnrollManager courseEnrollManager=new CourseEnrollManager();
        
        // GET: CourseEnroll
        [Authorize(Roles = "Teacher")]
        public ActionResult Index()
        {
            ViewBag.Title = "Enroll in a Course";
            ViewBag.Students = studentManager.GetAllstStudents();
            return View();
        }

        [Authorize(Roles = "Teacher")]
        [HttpPost]
        public ActionResult Index(CourseEnrollViewModel courseEnroll)//int StudentId, int courseId, DateTime date)
        {
            ViewBag.Msg = courseEnrollManager.EnrollCourse(courseEnroll);
            ViewBag.Title = "Enroll in a Course";
            ViewBag.Students = studentManager.GetAllstStudents();
            ModelState.Clear();
            return View();
        }

        [Authorize(Roles = "Teacher")]
        public ActionResult SaveStudentResult()
        {
            ViewBag.Title = "Save Student Result";
            ViewBag.Students = courseEnrollManager.GetEnrolledStudents();
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Teacher")]
        public ActionResult SaveStudentResult(int StudentId, int courseId, string grade)
        {
            ViewBag.Msg = courseEnrollManager.SaveResult(StudentId, courseId, grade);
            ViewBag.Title = "Save Student Result";
            ViewBag.Students = courseEnrollManager.GetEnrolledStudents();
            return View();
        }

        public ActionResult ViewStudentResult()
        {
            ViewBag.Title = "View Student Result.";
            ViewBag.Students = courseEnrollManager.GetEnrolledStudents();
            return View();
        }
        public JsonResult GetStudentById(int studentId)
        {
            var student = studentManager.GetStudentById(studentId);
            return Json(student, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCourseByStudentId(int studentId)
        {
            var student = studentManager.GetStudentById(studentId);
            var courses = courseManager.GetCourseByStudent(student);
            return Json(courses, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEnrollledCourseByStudentId(int studentId)
        {
            var courses = courseEnrollManager.GetEnrolledCourseByStudentId(studentId);
            return Json(courses, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetIncompletePrerequisite(int studentId, int courseId)
        {
            var prereq = courseEnrollManager.GetIncompletePrerequisite(studentId, courseId);
            return Json(prereq, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetResultById(int studentId)
        {
            var courses = courseEnrollManager.GetResultById(studentId);
            return Json(courses, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreatePdf(int studentId)
        {
            var result = courseEnrollManager.GetResultById(studentId);
            var student = studentManager.GetStudentById(studentId);

            //pdf er code
            var doc = new Document(PageSize.A4, 50, 50, 25, 25);
            var stream = new MemoryStream();

            PdfWriter writer = PdfWriter.GetInstance(doc, stream);

            //writer.PageEvent = new PDFHeader("Result");

            doc.Open();
            Paragraph heading = new Paragraph("Bangladesh University of Business & Technology\nResult", FontFactory.GetFont("Times New Roman", 20, Font.BOLD));
            heading.Alignment = Element.ALIGN_LEFT;
            doc.Add(heading);

            Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
            doc.Add(p);

            var HeaderTable = new PdfPTable(2);
            HeaderTable.HorizontalAlignment = 0;
            HeaderTable.SpacingBefore = 20;
            HeaderTable.SpacingAfter = 10;
            HeaderTable.DefaultCell.Border = 0;
            HeaderTable.SetWidths(new int[] { 3, 6 });

            HeaderTable.AddCell(new Phrase("Student Reg. No. ", FontFactory.GetFont("Times New Roman", 12, Font.BOLD)));
            HeaderTable.AddCell(new Phrase(student.RegistrationNo));
            HeaderTable.AddCell(new Phrase("Student Name ", FontFactory.GetFont("Times New Roman", 12, Font.BOLD)));
            HeaderTable.AddCell(new Phrase(student.Name));
            HeaderTable.AddCell(new Phrase("Student Email ", FontFactory.GetFont("Times New Roman", 12, Font.BOLD)));
            HeaderTable.AddCell(new Phrase(student.Email));
            HeaderTable.AddCell(new Phrase("Department ", FontFactory.GetFont("Times New Roman", 12, Font.BOLD)));
            HeaderTable.AddCell(new Phrase(student.DepartmentName.Name));

            doc.Add(HeaderTable);

            var DetailTable = new PdfPTable(3);
            DetailTable.HorizontalAlignment = 0;
            DetailTable.SpacingAfter = 10;
            DetailTable.DefaultCell.Border = 0;
            DetailTable.SetWidths(new int[] { 6, 10, 3 });
            DetailTable.WidthPercentage = 100;
            DetailTable.DefaultCell.Border = Rectangle.BOX;

            DetailTable.AddCell(new Phrase("Course Code", FontFactory.GetFont("Times New Roman", 12, Font.BOLD)));
            DetailTable.AddCell(new Phrase("Course Name", FontFactory.GetFont("Times New Roman", 12, Font.BOLD)));
            DetailTable.AddCell(new Phrase("Grade", FontFactory.GetFont("Times New Roman", 12, Font.BOLD)));

            foreach (var Line in result)
            {
                DetailTable.AddCell(new Phrase(Line.Code));
                DetailTable.AddCell(new Phrase(Line.CourseName));
                DetailTable.AddCell(new Phrase(Line.Grade));
            }
            doc.Add(DetailTable);

            doc.Add(p);

            Paragraph foot = new Paragraph("Develped By Team CodeRiders.");
            foot.Alignment = Element.ALIGN_LEFT;
            doc.Add(foot);

            doc.Close();
            //pdf er code sesh...

            //download er code
            byte[] bytes = stream.ToArray();
            stream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + student.RegistrationNo + ".pdf");
            Response.ContentType = "application/pdf";
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(bytes);
            Response.End();
            Response.Close();
            //download sesh...
            ModelState.Clear();
            return View();
        }
    }
}