﻿using System;
using System.Web.Mvc;
using UniversityManagementSystem.BLL;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Controllers
{
    public class ClassroomController : Controller
    {
        private DepartmentManager departmentManager = new DepartmentManager();
        private ScheduleManager scheduleManager = new ScheduleManager();
        private CourseManager courseManager = new CourseManager();

        // GET: Classroom
        [Authorize(Roles = "Teacher")]
        public ActionResult Index()
        {
            ViewBag.Title = "Allocate Classroom";
            ViewBag.Departments = departmentManager.GetallDepartments();
            ViewBag.Rooms = scheduleManager.GetAllRooms();
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Teacher")]
        public ActionResult Index(int courseId, int roomId, string day, int fromTimehr, int fromTimemn, string fromampm, int toTimehr, int toTimemn, string toampm)
        {
            ViewBag.Msg = scheduleManager.AllocateRoom(new Schedule()
            {
                CourseId = courseId,
                RoomId = roomId,
                Day = day,
                FromTime = DateTime.Parse(fromTimehr + ":" + fromTimemn + " " + fromampm),
                ToTime = DateTime.Parse(toTimehr + ":" + toTimemn + " " + toampm)
        });
            ViewBag.Title = "Allocate Classroom";
            ViewBag.Departments = departmentManager.GetallDepartments();
            ViewBag.Rooms = scheduleManager.GetAllRooms();
            ModelState.Clear();
            return View();
        }

        public ActionResult ViewClassSchedule()
        {
            ViewBag.Title = "Room Alocation Info";
            ViewBag.Departments = departmentManager.GetallDepartments();
            return View();
        }

        [Authorize(Roles = "Teacher")]
        public ActionResult UnallocateRooms()
        {
            ViewBag.Title = "Unallocate Rooms";
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Teacher")]
        public ActionResult UnallocateRooms(bool id)
        {
            if (id) ViewBag.Msg = scheduleManager.UnallocateAllRooms();
            ViewBag.Title = "Unallocate Rooms";
            return View();
        }

        public JsonResult GetScheduleByDept(int departmentId)
        {
            var schedules = scheduleManager.GetScheduleByDept(departmentId);
            return Json(schedules, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCourseByDept(int deptId)
        {
            var courses = courseManager.GetCourseByDept(deptId);
            return Json(courses, JsonRequestBehavior.AllowGet);
        }
    }
}