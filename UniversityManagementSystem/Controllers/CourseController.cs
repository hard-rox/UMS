﻿using System.Web.Mvc;
using UniversityManagementSystem.BLL;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Controllers
{
    public class CourseController : Controller
    {
        private CourseManager courseManager = new CourseManager();
        private DepartmentManager departmentManager = new DepartmentManager();
        private SemesterManager semesterManager = new SemesterManager();

        // GET: Course
        [Authorize(Roles = "Teacher")]
        public ActionResult SaveCourse()
        {
            ViewBag.Departments = departmentManager.GetallDepartments();
            ViewBag.Semesters = semesterManager.GetAllSemesters();
            ViewBag.Prereqs = courseManager.GetAllCourse();
            ViewBag.Title = "Save Course";
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Teacher")]
        public ActionResult SaveCourse(Course course)
        {
            ViewBag.Msg = courseManager.SaveCourse(course);
            ViewBag.Departments = departmentManager.GetallDepartments();
            ViewBag.Semesters = semesterManager.GetAllSemesters();
            ViewBag.Prereqs = courseManager.GetAllCourse();
            ViewBag.Title = "Save Course";
            ModelState.Clear();
            return View();
        }
    }
}